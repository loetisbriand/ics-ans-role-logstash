import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_logstash_running_and_enabled(host):
    logstash = host.service("logstash")
    assert logstash.is_running
    assert logstash.is_enabled
