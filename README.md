# ics-ans-role-logstash

Ansible role to install logstash.

## Role Variables

```yaml
logstash_gpg_key: https://packages.elastic.co/GPG-KEY-elasticsearch

centos_mirror_url: https://artifacts.elastic.co

logstash_version: 7.x

logstash_conf_template:
  - name: config
    file: iologstash.conf.j2
    dest: /etc/logstash/conf.d/iologstash.conf

logstash_repository:
  - baseurl: "{{ centos_mirror_url }}/packages/{{ logstash_version }}/yum"
    description: "Elastic repository for {{ logstash_version }} packages"
    enabled: true
    gpgcheck: true
    gpgkey: "{{ centos_mirror_url }}/GPG-KEY-elasticsearch"
    name: logstash
    reposdir: /etc/yum.repos.d/

logstash_output_host: localhost:9200

logstash_filebeat_input_port: 5044

logstash_output_index_name: filebeat

...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-logstash
```

## License

BSD 2-clause
